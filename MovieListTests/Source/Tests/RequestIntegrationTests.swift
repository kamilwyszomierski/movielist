//
//  RequestIntegrationTests.swift
//  MovieListTests
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import XCTest
@testable import MovieList

class RequestIntegrationTests: XCTestCase {

	func test_givenPlayingNowRequestWithoutAPIKey_whereGetCalled_thenReturnsReponseWithError() throws {

		// given
		let expect = expectation(description: "Expect completion call.")
		let url = try XCTUnwrap(URL(baseURLString: RemoteConfiguration.API.baseURL, version: RemoteConfiguration.API.version, endpoint: .nowPlaying))

		// when
		_ = Request().get(dataOf: String.self, url: url) { result in
			expect.fulfill()
			switch result {
			case .failure: break
			case .success: XCTFail()
			}
		}

		// then
		wait(for: [expect], timeout: 35.0)
	}

	func test_givenPlayingNowRequest_whereGetCalled_thenReturnsReponseWithSuccess() throws {

		// given
		let expect = expectation(description: "Expect completion call.")
		let parameters = NowPlayingParameters(page: 1)
		let url = try XCTUnwrap(URL(baseURLString: RemoteConfiguration.API.baseURL, version: RemoteConfiguration.API.version, endpoint: .nowPlaying))

		// when
		_ = Request().get(dataOf: NowPlayingResponse.self, url: url, parameters: parameters) { result in
			expect.fulfill()
			switch result {
			case .failure: XCTFail()
			case .success: break
			}
		}

		// then
		wait(for: [expect], timeout: 35.0)
	}
}
