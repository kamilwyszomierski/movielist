//
//  ImageDonwloaderIntegrationTests.swift
//  MovieListTests
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import XCTest
@testable import MovieList

class ImageDonwloaderIntegrationTests: XCTestCase {

	func test_givenImageURL_whereGetImageCalled_thenReturnsReponseWithImage() throws {

		// given
		let expect = expectation(description: "Expect completion call.")
		let path = "/jHo2M1OiH9Re33jYtUQdfzPeUkx.jpg"
		let url = try XCTUnwrap(URL(posterBaseURLString: RemoteConfiguration.API.imageBaseURL, size: .original, path: path))

		// when
		// then
		_ = ImageDownloader().getImage(from: url) { image in
			expect.fulfill()
			XCTAssertNotNil(image)
		}

		wait(for: [expect], timeout: 35.0)
	}
}
