//
//  MovieDetailsViewController.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

final class MovieDetailsViewController: UIViewController, BackedViewProvider {

    typealias View = MovieDetailsView

	private var viewModel: MovieDetailsViewModel

	init(viewModel: MovieDetailsViewModel) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func loadView() {
		view = MovieDetailsView()
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		navigationItem.title = viewModel.navigationTitle
		navigationItem.rightBarButtonItem = UIBarButtonItem(customView: backedView.favouriteButton)
		backedView.apply(viewModel: viewModel)
		viewModel.viewLoaded()

		backedView.favouriteButton.apply(style: viewModel.isFavourite ? .favourite : .unfavourite)
		viewModel.onFavouriteChanged = { [weak self] in
			self?.backedView.favouriteButton.apply(style: $0 ? .favourite : .unfavourite)
		}

		backedView.favouriteButton.addTarget(self, action: #selector(didTouchUpInside(favouriteButton:)), for: .touchUpInside)
	}

	@objc private func didTouchUpInside(favouriteButton: UIButton) {
		viewModel.didTouchFavourite()
	}
}

// MARK: - Preview

#if canImport(SwiftUI) && DEBUG
import SwiftUI

@available(iOS 13.0.0, *)
extension MovieDetailsViewController: UIViewControllerRepresentable { }

@available(iOS 13.0.0, *)
struct MovieDetailsViewControllerPreview: PreviewProvider {

	static var previews: some View {
		Group {
			ForEach(ColorScheme.allCases, id: \.self) { colorScheme in
				MovieDetailsViewController(viewModel: Self.makePreviewViewModel())
					.edgesIgnoringSafeArea(.all)
					.environment(\.colorScheme, colorScheme)
					.previewDisplayName("\(colorScheme)")
			}
		}
	}

	private static func makePreviewViewModel() -> MovieDetailsViewModel {
		return MovieDetailsViewModel(
			posterURL: .image(UIImage(named: "avengers")!),
			voteAverageLabel: "Avarage vote: 7.8",
			titleLabel: "Avengers: Endgame",
			overviewLabel: "After the devastating events of Avengers: Infinity War, the universe is in ruins due to the efforts of the Mad Titan, Thanos. With the help of remaining allies, the Avengers must assemble once more in order to undo Thanos' actions and restore order to the universe once and for all, no matter what consequences may be in store.",
			releaseDateLabel: "Jan 8, 2020",
			isFavourite: true
		)
	}
}
#endif
