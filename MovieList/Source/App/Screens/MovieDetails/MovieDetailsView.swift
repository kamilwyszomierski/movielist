//
//  MovieDetailsView.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

class MovieDetailsView: UIView {

	let posterImageView = NetworkImageView.make {
		$0.backgroundColor = .groupTableViewBackground
		$0.contentMode = .scaleAspectFill
		$0.clipsToBounds = true
	}

	let releaseDataLabel = UILabel.make {
		$0.apply(style: .subheader)
	}

	let titleLabel = UILabel.make {
		$0.apply(style: .title)
	}

	let voteAverageLabel = UILabel.make {
		$0.apply(style: .subheader)
	}

	let overviewLabel = UILabel.make {
		$0.apply(style: .body)
		$0.numberOfLines = 0
	}

	let favouriteButton = UIButton(type: .system)

	private let mainStackView = UIStackView.make {
		$0.alignment = .top
		$0.axis = .vertical
		$0.spacing = 8.0
	}

	private let contentView = UIView()
	private let scrollView = UIScrollView()

	override init(frame: CGRect) {
		super.init(frame: frame)

		setupViews()
		mainStackView.addArrangedSubviews([
			posterImageView,
			titleLabel,
			releaseDataLabel,
			voteAverageLabel,
			overviewLabel
		])

		setupConstraints()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setupViews() {
		backgroundColor = .white
		addSubviewWithoutAutoresizingMask(scrollView)
		scrollView.addSubviewWithoutAutoresizingMask(contentView)
		contentView.addSubviewWithoutAutoresizingMask(mainStackView)
	}

	private func setupConstraints() {
		NSLayoutConstraint.activate([
			scrollView.topAnchor.constraint(equalTo: topAnchor),
			scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
			scrollView.bottomAnchor.constraint(equalTo: bottomAnchor),
			scrollView.trailingAnchor.constraint(equalTo: trailingAnchor)
		])

		NSLayoutConstraint.activate([
			contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
			contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
			contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
			contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
			contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
		])

		let contentHeightConstraint = contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
		contentHeightConstraint.priority = .lessDefaultLow
		contentHeightConstraint.isActive = true

		NSLayoutConstraint.activate([
			mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
			mainStackView.leadingAnchor.constraint(equalTo: readableContentGuide.leadingAnchor),
			mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
			mainStackView.trailingAnchor.constraint(equalTo: readableContentGuide.trailingAnchor)
		])

		NSLayoutConstraint.activate([
			posterImageView.heightAnchor.constraint(equalTo: scrollView.heightAnchor, multiplier: 0.7)
		])

		NSLayoutConstraint.activate([
			favouriteButton.widthAnchor.constraint(equalToConstant: 24),
			favouriteButton.heightAnchor.constraint(equalToConstant: 24)
		])

		setNeedsUpdateConstraints()
	}
}

// MARK: - ViewModelApplicable

extension MovieDetailsView: ViewModelApplicable {

	func apply(viewModel: MovieDetailsViewModel) {
		overviewLabel.text = viewModel.overviewLabel
		releaseDataLabel.text = viewModel.releaseDateLabel
		titleLabel.text = viewModel.titleLabel
		voteAverageLabel.text = viewModel.voteAverageLabel

		switch viewModel.posterURL {
		case .image(let image):
			posterImageView.image = image

		case .url(let url):
			posterImageView.getImage(from: url)

		default:
			posterImageView.image = nil
		}
	}
}
