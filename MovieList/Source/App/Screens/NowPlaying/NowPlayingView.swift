//
//  NowPlayingView.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

class NowPlayingView: UIView {

	let tableView = UITableView()

	override init(frame: CGRect) {
		super.init(frame: frame)

		setupViews()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	private func setupViews() {
		addSubviewWithoutAutoresizingMask(tableView)
		tableView.tableFooterView = UIView()
		tableView.backgroundColor = .white
	}
	
	private func setupConstraints() {
		NSLayoutConstraint.activate([
			tableView.topAnchor.constraint(equalTo: topAnchor),
			tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
			tableView.bottomAnchor.constraint(equalTo: bottomAnchor),
			tableView.trailingAnchor.constraint(equalTo: trailingAnchor)
		])
		
		setNeedsUpdateConstraints()
	}
}
