//
//  NowPlayingViewController.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

final class NowPlayingViewController: UIViewController, BackedViewProvider {

	typealias View = NowPlayingView

	private var viewModel: NowPlayingViewModel

	init(viewModel: NowPlayingViewModel) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func loadView() {
		view = NowPlayingView()
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		setupTableView()
		setupNavigationView()
		viewModel.viewLoaded()
	}
}

// MARK: - Setup

extension NowPlayingViewController {

	private func setupNavigationView() {
		navigationItem.title = viewModel.title
	}

	private func setupTableView() {
		backedView.tableView.register(MovieTableCell.self, forCellReuseIdentifier: MovieTableCell.reuseIdentifier)
		backedView.tableView.dataSource = self
		backedView.tableView.delegate = self
		viewModel.onDataLoaded = { [weak self] in
			self?.backedView.tableView.reloadData()
		}
	}
}

// MARK: - UITableViewDataSource

extension NowPlayingViewController: UITableViewDataSource {

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.cellViewModels.count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let tableCell = tableView.dequeueReusableCell(withIdentifier: MovieTableCell.reuseIdentifier, for: indexPath)
		guard let movieCell = tableCell as? MovieTableCell else { return tableCell }
		movieCell.delegate = self
		movieCell.apply(viewModel: viewModel.cellViewModels[indexPath.row])
		return movieCell
	}
}

// MARK: - UITableViewDelegate

extension NowPlayingViewController: UITableViewDelegate {

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		viewModel.didSelectCell(at: indexPath)
	}
}

// MARK: - UIScrollViewDelegate

extension NowPlayingViewController: UIScrollViewDelegate {

	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let triggerOffset = scrollView.frame.size.height * 3
		let bottomOffset = scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.frame.size.height
		let shouldRequestForNextPage = bottomOffset < triggerOffset
		guard shouldRequestForNextPage else { return }
		viewModel.didScrollToNextPage()
	}
}

// MARK: - MovieTableCellDelegate

extension NowPlayingViewController: MovieTableCellDelegate {

	func didTouchFavouriteButton(_ cell: MovieTableCell) {
		guard let indexPath = backedView.tableView.indexPath(for: cell) else { return }
		viewModel.didSelectCellAsFavourite(at: indexPath)
	}
}

// MARK: - Preview

#if canImport(SwiftUI) && DEBUG
import SwiftUI

@available(iOS 13.0.0, *)
extension NowPlayingViewController: UIViewControllerRepresentable { }

@available(iOS 13.0.0, *)
struct NowPlayingViewControllerPreview: PreviewProvider {

	static var previews: some View {
		Group {
			ForEach(ColorScheme.allCases, id: \.self) { colorScheme in
				NowPlayingViewController(viewModel: Self.makePreviewViewModel())
					.edgesIgnoringSafeArea(.all)
					.environment(\.colorScheme, colorScheme)
					.previewDisplayName("\(colorScheme)")
			}
		}
	}

	private static func makePreviewViewModel() -> NowPlayingViewModel {
		return NowPlayingViewModel(cellViewModels: [MovieTableCellPreview.viewModel])
	}
}
#endif
