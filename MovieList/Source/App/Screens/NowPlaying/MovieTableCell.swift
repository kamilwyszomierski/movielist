//
//  MovieTableCell.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

protocol MovieTableCellDelegate: AnyObject {

	func didTouchFavouriteButton(_ cell: MovieTableCell)
}

class MovieTableCell: UITableViewCell, CellIdentifiable {

	weak var delegate: MovieTableCellDelegate?

	let voteAverage = UILabel.make {
		$0.apply(style: .subheader)
	}

	let titleLabel = UILabel.make {
		$0.apply(style: .header)
	}

	let posterImage = NetworkImageView.make {
		$0.backgroundColor = .groupTableViewBackground
		$0.contentMode = .scaleAspectFill
		$0.clipsToBounds = true
	}

	let favouriteButton = UIButton(type: .system)

	private let mainStackView = UIStackView.make {
		$0.alignment = .center
		$0.axis = .horizontal
		$0.spacing = 8.0
	}

	private let descriptionStackView = UIStackView.make {
		$0.alignment = .top
		$0.axis = .vertical
	}

	private var isFavourite = false

	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)

		setupViews()
		mainStackView.addArrangedSubviews([
			posterImage,
			descriptionStackView,
			favouriteButton
		])

		descriptionStackView.addArrangedSubviews([
			titleLabel,
			voteAverage
		])

		setupConstraints()
	}

	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

// MARK: - Setup

extension MovieTableCell {

	private func setupViews() {
		backgroundColor = .white
		contentView.backgroundColor = .white
		contentView.addSubviewWithoutAutoresizingMask(mainStackView)

		favouriteButton.addTarget(self, action: #selector(didTouchUpInside(favouriteButton:)), for: .touchUpInside)
	}

	private func setupConstraints() {
		NSLayoutConstraint.activate([
			mainStackView.topAnchor.constraint(equalTo: contentView.topAnchor),
			mainStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
			mainStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
			mainStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8.0)
		])

		NSLayoutConstraint.activate([
			posterImage.widthAnchor.constraint(equalToConstant: 80.0),
			posterImage.heightAnchor.constraint(equalToConstant: 120.0)
		])

		NSLayoutConstraint.activate([
			favouriteButton.widthAnchor.constraint(equalToConstant: 34),
			favouriteButton.heightAnchor.constraint(equalToConstant: 34)
		])

		setNeedsUpdateConstraints()
	}
}

// MARK: - Actions

extension MovieTableCell {

	@objc private func didTouchUpInside(favouriteButton: UIButton) {
		isFavourite = !isFavourite
		favouriteButton.apply(style: isFavourite ? .favourite : .unfavourite)
		delegate?.didTouchFavouriteButton(self)
	}
}

// MARK: - ViewModelApplicable

extension MovieTableCell: ViewModelApplicable {

	func apply(viewModel: MovieCellViewModel) {
		isFavourite = viewModel.isFavourite
		switch viewModel.posterURL {
		case .image(let image):
			posterImage.image = image

		case .url(let url):
			posterImage.getImage(from: url)

		default:
			posterImage.image = nil
		}

		voteAverage.text = viewModel.voteAverageLabel
		titleLabel.text = viewModel.titleLabel
		favouriteButton.apply(style: isFavourite ? .favourite : .unfavourite)
	}
}

// MARK: - Preview

#if canImport(SwiftUI) && DEBUG
import SwiftUI

@available(iOS 13.0.0, *)
struct MovieTableCellPreview: PreviewProvider {

	static let viewModel = MovieCellViewModel(
		voteAverageLabel: "Avarage vote: 7.8",
		titleLabel: "Avengers",
		posterURL: .image(UIImage(named: "avengers")!),
		isFavourite: true
	)

	private static func makeCell(viewModel: MovieCellViewModel) -> MovieTableCell {
		return .make {
			$0.apply(viewModel: viewModel)
		}
	}

	static var previews: some View {
		Group {
			ForEach(ColorScheme.allCases, id: \.self) { (colorScheme) in
				UIViewPreview {
					makeCell(viewModel: viewModel)
				}
					.environment(\.colorScheme, colorScheme)
					.previewDisplayName("\(colorScheme)")
					.previewLayout(.fixed(width: 375, height: 100))
			}
		}
	}
}
#endif
