//
//  AppDelegate.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		let navigationController = UINavigationController()
		if #available(iOS 11.0, *) {
			navigationController.navigationBar.prefersLargeTitles = true
		}

		let viewModel = NowPlayingViewModel()
		viewModel.onMovieSelected = {
			let detailsViewModel = MovieDetailsViewModel(movie: $0)
			let detailsViewController = MovieDetailsViewController(viewModel: detailsViewModel)
			navigationController.show(detailsViewController, sender: nil)
		}

		let viewController = NowPlayingViewController(viewModel: viewModel)
		navigationController.setViewControllers([viewController], animated: false)

		let startWindow = UIWindow(frame: UIScreen.main.bounds)
		startWindow.rootViewController = navigationController
		startWindow.makeKeyAndVisible()
		window = startWindow

		return true
	}
}
