//
//  AppComponents.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 25/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

class AppComponents {

	static let favouriteMoviesStorage: FavouriteMoviesStorageProtocol = FavouriteMoviesStorage()
	static let urlSession = URLSession(configuration: .default)
}
