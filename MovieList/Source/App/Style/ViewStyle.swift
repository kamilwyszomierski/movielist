//
//  ViewStyle.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct ViewStyle<T> {

	let style: ValueClosure<T>

	func compose(with style: ViewStyle<T>) -> ViewStyle<T> {
		return ViewStyle<T> {
			self.style($0)
			style.style($0)
		}
	}
}
