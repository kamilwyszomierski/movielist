//
//  LabelStyle.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

extension UILabel: Stylable { }

extension ViewStyle where T: UILabel {

	static var common: ViewStyle<T> {
		ViewStyle<T> {
			$0.adjustsFontForContentSizeCategory = true
			$0.textColor = .darkText
		}
	}

	static var header: ViewStyle<T> {
		common.compose(with: ViewStyle<T> {
			$0.font = UIFont.preferredFont(forTextStyle: .headline)
		})
	}

	static var subheader: ViewStyle<T> {
		common.compose(with: ViewStyle<T> {
			$0.font = UIFont.preferredFont(forTextStyle: .subheadline)
		})
	}

	static var title: ViewStyle<T> {
		common.compose(with: ViewStyle<T> {
			$0.font = UIFont.preferredFont(forTextStyle: .title1)
		})
	}

	static var body: ViewStyle<T> {
		common.compose(with: ViewStyle<T> {
			$0.font = UIFont.preferredFont(forTextStyle: .body)
		})
	}
}
