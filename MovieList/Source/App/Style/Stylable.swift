//
//  Stylable.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

protocol Stylable { }

extension Stylable {

	func apply(style: ViewStyle<Self>) {
		style.style(self)
	}
}
