//
//  ButtonStyle.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

extension UIButton: Stylable { }

extension ViewStyle where T: UIButton {

	static var favourite: ViewStyle<T> {
		ViewStyle<T> {
			$0.setImage(UIImage(named: "star.fill.color"), for: .normal)
		}
	}

	static var unfavourite: ViewStyle<T> {
		ViewStyle<T> {
			$0.setImage(UIImage(named: "star.empty"), for: .normal)
		}
	}
}
