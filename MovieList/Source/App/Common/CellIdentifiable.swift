//
//  CellIdentifiable.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

protocol CellIdentifiable {

	static var reuseIdentifier: String { get }
}

extension CellIdentifiable {

	static var reuseIdentifier: String {
		return "\(Self.self)"
	}
}
