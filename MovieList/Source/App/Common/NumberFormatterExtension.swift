//
//  NumberFormatterExtension.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

extension NumberFormatter {

	static let voteFormat = NumberFormatter.make {
		$0.numberStyle = .decimal
		$0.maximumFractionDigits = 2
		$0.minimumFractionDigits = 2
	}
}
