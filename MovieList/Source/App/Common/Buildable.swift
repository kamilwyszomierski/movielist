//
//  Buildable.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

protocol Buildable { }

extension NSObject: Buildable { }

extension Buildable where Self: NSObject {

	static func make(builder: ValueClosure<Self>) -> Self {
		let object = Self()
		builder(object)
		return object
	}
}
