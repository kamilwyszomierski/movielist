//
//  NetworkImageView.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

class NetworkImageView: UIImageView {

	let imageDownloader = ImageDownloader()

	private(set) var cancellable: Cancellable?
	private(set) var downloadedImageURL: URLConvertible?

	func getImage(from url: URLConvertible?, completion: ValueClosure<UIImage?>? = nil) {
		guard let url = url else {
			cancellable?.cancel()
			image = nil
			return
		}

		guard !isURLSameAsDownloading(url: url) else { return }
		cancellable?.cancel()
		image = nil

		cancellable = imageDownloader.getImage(from: url) { [weak self] response in
			guard let self = self else { return }
			let url = response.url.absoluteString

			guard !self.isURLSameAsDownloading(url: url) else { return }
			switch response.result {
			case.failure:
				self.image = nil
				completion?(nil)

			case .success(let image):
				self.image = image
				completion?(image)
			}

			self.cancellable = nil
		}
	}

	private func isURLSameAsDownloading(url: URLConvertible) -> Bool {
		let url = try? url.toURL().absoluteString
		let currentURL = try? downloadedImageURL?.toURL().absoluteString
		return url == currentURL
	}
}
