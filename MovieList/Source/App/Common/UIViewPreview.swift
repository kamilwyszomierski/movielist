//
//  UIViewPreview.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

#if canImport(SwiftUI) && DEBUG
import SwiftUI
struct UIViewPreview<View: UIView> {

    let view: View

    init(_ builder: @escaping () -> View) {
        view = builder()
    }
}

// MARK: - UIViewRepresentable

extension UIViewPreview: UIViewRepresentable {

    func makeUIView(context: Context) -> UIView {
        return view
    }

    func updateUIView(_ view: UIView, context: Context) {
		view.setContentHuggingPriority(.important, for: .horizontal)
        view.setContentHuggingPriority(.important, for: .vertical)
    }
}
#endif
