//
//  Closure.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

typealias Closure = () -> Void
typealias ValueClosure<T> = (_ value: T) -> Void
