//
//  Cancellable.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

protocol Cancellable {

	func cancel()
}

extension Cancellable {

	func store(in collection: inout Array<Cancellable>) {
		collection.append(self)
	}
}
