//
//  Poster.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

enum Poster {
	case image(UIImage)
	case url(URLConvertible)
}
