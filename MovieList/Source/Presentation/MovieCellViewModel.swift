//
//  MovieCellViewModel.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct MovieCellViewModel {

	let voteAverageLabel: String
	let titleLabel: String
	let posterURL: Poster?
	var isFavourite: Bool

	init(voteAverageLabel: String, titleLabel: String, posterURL: Poster, isFavourite: Bool) {
		self.voteAverageLabel = voteAverageLabel
		self.titleLabel = titleLabel
		self.posterURL = posterURL
		self.isFavourite = isFavourite
	}

	init(movie: Movie, isFavourite: Bool) {
		let formattedVote = NumberFormatter.voteFormat.string(from: movie.voteAverage as NSNumber) ?? ""
		self.voteAverageLabel = "Average vote: \(formattedVote)"
		self.titleLabel = movie.title
		self.isFavourite = isFavourite

		if
			let posterPath = movie.posterPath,
			let url = URL(posterBaseURLString: RemoteConfiguration.API.imageBaseURL, size: .w500, path: posterPath) {
			self.posterURL = .url(url)
		} else {
			self.posterURL = nil
		}
	}
}
