//
//  NowPlayingViewModel.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

class NowPlayingViewModel {

	var onDataLoaded: Closure?
	var onMovieSelected: ValueClosure<Movie>?
	let title = "Now playing"

	private(set) var cellViewModels = [MovieCellViewModel]()
	private(set) var isLoading = false
	private(set) var currentlyLoadedPage = 0

	private var cancellables = [Cancellable]()
	private var favouriteMoviesStorage: FavouriteMoviesStorageProtocol?
	private var movies = [Movie]()

	init(cellViewModels: [MovieCellViewModel]) {
		self.cellViewModels = cellViewModels
	}

	init(favouriteMoviesStorage: FavouriteMoviesStorageProtocol = AppComponents.favouriteMoviesStorage) {
		self.favouriteMoviesStorage = favouriteMoviesStorage
	}

	func viewLoaded() {
		getData()
	}

	func viewDisappeared() {
		cancellables.forEach { $0.cancel() }
	}

	func didSelectCell(at indexPath: IndexPath) {
		onMovieSelected?(movies[indexPath.row])
	}

	func didScrollToNextPage() {
		getData()
	}

	private func getData() {
		guard !isLoading else { return }
		isLoading = true
		guard let url = URL(baseURLString: RemoteConfiguration.API.baseURL, version: RemoteConfiguration.API.version, endpoint: .nowPlaying) else { return }

		currentlyLoadedPage += 1

		var ids = [ID<Movie>]()
		var movies = [Movie]()
		let group = DispatchGroup()
		group.enter()
		let parameters = NowPlayingParameters(page: currentlyLoadedPage)
		Request().get(dataOf: NowPlayingResponse.self, url: url, parameters: parameters) { [weak self] result in
			guard let self = self else { return }
			switch result {
			case .failure: return
			case .success(let response):
				self.currentlyLoadedPage = response.page
				movies = response.results
			}
			group.leave()
		}?.store(in: &cancellables)

		group.enter()
		favouriteMoviesStorage?.getIDs(completion: { result in
			switch result {
			case .failure: break
			case .success(let receivedIDs):
				ids = receivedIDs
			}
			group.leave()
		})

		group.notify(queue: .main) { [weak self] in
			guard let self = self else { return }
			self.isLoading = false
			let models = movies.map { MovieCellViewModel(movie: $0, isFavourite: ids.contains($0.id)) }
			self.cellViewModels.append(contentsOf: models)
			self.movies.append(contentsOf: movies)
			self.onDataLoaded?()
		}
	}

	func didSelectCellAsFavourite(at indexPath: IndexPath) {
		let isFavourite = !cellViewModels[indexPath.row].isFavourite
		cellViewModels[indexPath.row].isFavourite = isFavourite
		favouriteMoviesStorage?.setMovie(with: movies[indexPath.row].id, asFavourite: isFavourite)
	}
}
