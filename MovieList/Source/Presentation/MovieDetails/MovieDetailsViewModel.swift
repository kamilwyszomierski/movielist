//
//  MovieDetailsViewModel.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

class MovieDetailsViewModel {

	let navigationTitle = "Details"
	let posterURL: Poster?
	let voteAverageLabel: String
	let titleLabel: String
	let overviewLabel: String
	let releaseDateLabel: String

	var onFavouriteChanged: ValueClosure<Bool>?

	private(set) var isFavourite: Bool

	private var favouriteMoviesStorage: FavouriteMoviesStorageProtocol?
	private var movie: Movie?

	init(
		posterURL: Poster?,
		voteAverageLabel: String,
		titleLabel: String,
		overviewLabel: String,
		releaseDateLabel: String,
		isFavourite: Bool
	) {
		self.posterURL = posterURL
		self.voteAverageLabel = voteAverageLabel
		self.titleLabel = titleLabel
		self.overviewLabel = overviewLabel
		self.releaseDateLabel = releaseDateLabel
		self.isFavourite = isFavourite
	}

	init(movie: Movie, favouriteMoviesStorage: FavouriteMoviesStorageProtocol = AppComponents.favouriteMoviesStorage) {
		let formattedVote = NumberFormatter.voteFormat.string(from: movie.voteAverage as NSNumber) ?? ""
		self.voteAverageLabel = "Average vote: \(formattedVote)"
		self.titleLabel = movie.title
		self.overviewLabel = movie.overview
		self.releaseDateLabel = DateFormatter.localizedString(from: movie.releaseDate, dateStyle: .medium, timeStyle: .none)
		self.isFavourite = false
		self.favouriteMoviesStorage = favouriteMoviesStorage
		self.movie = movie

		if
			let posterPath = movie.posterPath,
			let url = URL(posterBaseURLString: RemoteConfiguration.API.imageBaseURL, size: .w500, path: posterPath) {
			self.posterURL = .url(url)
		} else {
			self.posterURL = nil
		}
	}

	func viewLoaded() {
		favouriteMoviesStorage?.getIDs(completion: { [weak self] result in
			switch result {
			case .failure: break
			case .success(let ids):
				guard let movie = self?.movie else { return }
				let isFavourite = ids.contains(movie.id)
				self?.isFavourite = isFavourite
				self?.onFavouriteChanged?(isFavourite)
			}
		})
	}

	func didTouchFavourite() {
		guard let id = movie?.id else { return }
		isFavourite = !isFavourite
		favouriteMoviesStorage?.setMovie(with: id, asFavourite: isFavourite)
		onFavouriteChanged?(isFavourite)
	}
}
