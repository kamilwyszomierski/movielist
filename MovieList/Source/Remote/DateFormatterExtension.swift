//
//  DateFormatterExtension.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

extension DateFormatter {

	static let movieDBReleaseDateFormatter: DateFormatter = {
		var formatter = DateFormatter()
		formatter.dateFormat = "YYYY-MM-DD"
		return formatter
	}()
}
