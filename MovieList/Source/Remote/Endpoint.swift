//
//  Endpoint.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

enum Endpoint: String {
	case nowPlaying = "movie/now_playing"
	case searchMovie = "search/movie"
}
