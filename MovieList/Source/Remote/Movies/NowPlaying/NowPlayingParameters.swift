//
//  NowPlayingParameters.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct NowPlayingParameters: URLParameters, Encodable {

	let apiKey = RemoteConfiguration.API.key
	let page: Int
}
