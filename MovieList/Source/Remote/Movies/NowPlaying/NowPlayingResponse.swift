//
//  NowPlayingResponse.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct NowPlayingResponse: Decodable {

	let results: [Movie]
	let page: Int
	let totalPages: Int
}

struct Movie: Decodable {

	let popularity: Double
	let posterPath: String?
	let id: ID<Movie>
	let title: String
	let voteAverage: Double
	let overview: String
	let releaseDate: Date
}
