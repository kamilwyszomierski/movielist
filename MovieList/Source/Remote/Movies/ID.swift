//
//  ID.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct ID<Subject>: Codable, Identifiable {

	let rawValue: Int

	init(rawValue: Int) {
		self.rawValue = rawValue
	}
}
