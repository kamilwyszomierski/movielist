//
//  JSONDecoderExtension.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 25/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

extension JSONDecoder {

	static let movieDB: JSONDecoder = {
		let decoder = JSONDecoder()
		decoder.keyDecodingStrategy = .convertFromSnakeCase
		decoder.dateDecodingStrategy = .formatted(.movieDBReleaseDateFormatter)
		return decoder
	}()
}
