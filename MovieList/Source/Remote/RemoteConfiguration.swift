//
//  RemoteConfiguration.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

enum RemoteConfiguration { }

extension RemoteConfiguration {

	struct API {
		static let baseURL = "https://api.themoviedb.org"
		static let imageBaseURL = "https://image.tmdb.org/t/p"
		static let key = "9f10e24220ad0b646f7988a457d71993"
		static let version = "3"
	}

	struct TimeInterval {
		static let `default` = 30.0
	}
}
