//
//  PosterSize.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

enum PosterSize: String {
	case w92
	case w154
	case w185
	case w342
	case w500
	case w780
	case original
}
