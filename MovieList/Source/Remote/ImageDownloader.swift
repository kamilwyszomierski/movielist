//
//  ImageDownloader.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

class ImageDownloader {

	private let session: URLSession

	init(session: URLSession = AppComponents.urlSession) {
		self.session = session
	}

	func getImage(from url: URLConvertible?, completion: @escaping ValueClosure<(result: Result<UIImage, APIError>, url: URL)>) -> Cancellable? {
		guard let url = try? url?.toURL() else { return nil }
		let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: RemoteConfiguration.TimeInterval.default)
		let task = session.dataTask(with: request) { data, response, error in
			Request.makeResult(
				data: data,
				converter: ImageConverter(),
				response: response,
				error: error,
				completion: { (result: Result<UIImage, APIError>) in
					completion((result, url))
				}
			)
		}

		task.resume()

		return task
	}
}
