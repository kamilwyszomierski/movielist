//
//  APIError.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct APIError: Decodable, Error {

	enum CodingKeys: String, CodingKey {
		case code = "statusCode"
		case isSucceed = "success"
		case message = "statusMessage"
	}

	let code: Int
	let isSucceed: Bool
	let message: String
}
