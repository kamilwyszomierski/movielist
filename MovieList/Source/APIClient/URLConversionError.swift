//
//  URLConversionError.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct URLConversionError: Error {

	let localizedDescription: String

	static func cannotConvertToURL(_ value: URLConvertible) -> URLConversionError {
		return URLConversionError(localizedDescription: "Cannot convert value: \(value) to URL")
	}
}
