//
//  JSONConverter.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct JSONConverter<Destination>: DataConverting where Destination: Decodable {

	let decoder: JSONDecoder

	init(decoder: JSONDecoder) {
		self.decoder = decoder
	}

	func convert(data: Data) throws -> Destination {
		try decoder.decode(Destination.self, from: data)
	}
}
