//
//  Identifiable.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

protocol Identifiable: Hashable where Self.RawValue: Hashable {

	associatedtype RawValue
	associatedtype Subject

	var rawValue: RawValue { get }

	init(rawValue: RawValue)
}

extension Identifiable where Self: Codable, Self.RawValue: Codable {

	init(from decoder: Decoder) throws {
		let container = try decoder.singleValueContainer()
		let rawValue = try container.decode(RawValue.self)
		self.init(rawValue: rawValue)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.singleValueContainer()
		try container.encode(rawValue)
	}
}
