//
//  URLConvertible.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

protocol URLConvertible {

	func toURL() throws -> URL
}

extension String: URLConvertible {

	func toURL() throws -> URL {
		guard let url = URL(string: self) else { throw URLConversionError.cannotConvertToURL(self) }
		return url
	}
}

extension URL: URLConvertible {

	func toURL() throws -> URL {
		return self
	}
}

extension URLRequest: URLConvertible {

	func toURL() throws -> URL {
		guard let url = url else { throw URLConversionError.cannotConvertToURL(self) }
		return url
	}
}
