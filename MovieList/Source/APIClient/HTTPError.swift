//
//  HTTPError.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 25/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

enum HTTPStatusCode: Int {
	case ok = 200
	case unauthorized = 401
}
