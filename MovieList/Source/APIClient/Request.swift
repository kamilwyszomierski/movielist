//
//  Request.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct Request {

	private let session: URLSession

	init(session: URLSession = AppComponents.urlSession) {
		self.session = session
	}

	func get<ResponseData>(
		dataOf type: ResponseData.Type,
		url: URLConvertible,
		completion: @escaping ValueClosure<Result<ResponseData, APIError>>
	) -> Cancellable? where ResponseData: Decodable {
		guard let url = Self.convertToURL(url) else { return nil }
		let request = Self.makeURLRequest(url: url, body: nil)
		let task = session.dataTask(with: request) { data, response, error in
			Self.makeResult(
				data: data,
				converter: JSONConverter<ResponseData>(decoder: .movieDB),
				response: response,
				error: error,
				completion: completion
			)
		}
		task.resume()

		return task
	}

	func get<Parameters, ResponseData>(
		dataOf type: ResponseData.Type,
		url: URLConvertible,
		parameters: Parameters,
		completion: @escaping ValueClosure<Result<ResponseData, APIError>>
	) -> Cancellable? where Parameters: Encodable & URLParameters, ResponseData: Decodable {
		guard let url = Self.convertToURL(url) else { return nil }
		let modifiedURL = Self.urlByAppending(parameters: parameters, to: url)
		let request = Self.makeURLRequest(url: modifiedURL, body: nil)
		let task = session.dataTask(with: request) { data, response, error in
			Self.makeResult(
				data: data,
				converter: JSONConverter<ResponseData>(decoder: .movieDB),
				response: response,
				error: error,
				completion: completion
			)
		}
		task.resume()

		return task
	}

	func get<Parameters, Body, ResponseData>(
		dataOf type: ResponseData.Type,
		url: URLConvertible,
		parameters: Parameters,
		body: Body,
		completion: @escaping ValueClosure<Result<ResponseData, APIError>>
	) -> Cancellable? where Parameters: Encodable & URLParameters, Body: Encodable, ResponseData: Decodable {
		guard let url = Self.convertToURL(url) else { return nil }
		let modifiedURL = Self.urlByAppending(parameters: parameters, to: url)
		let request = Self.makeURLRequest(url: modifiedURL, body: nil)
		let task = session.dataTask(with: request) { data, response, error in
			Self.makeResult(
				data: data,
				converter: JSONConverter<ResponseData>(decoder: .movieDB),
				response: response,
				error: error,
				completion: completion
			)
		}
		task.resume()

		return task
	}

	static func convertToURL(_ url: URLConvertible) -> URL? {
		do {
			return try url.toURL()
		} catch {
			assertionFailure("Cannot create url from \(url)")
			return nil
		}
	}

	static func urlByAppending<T>(parameters: T, to url: URL) -> URL where T: Encodable & URLParameters {
		let queryItems = Self.makeURLItems(from: parameters)
		guard var components = URLComponents(string: url.absoluteString) else {
			assertionFailure("Cannot make components from \(url.absoluteString)")
			return url
		}

		if components.queryItems == nil {
			components.queryItems = queryItems
		} else {
			components.queryItems?.append(contentsOf: queryItems)
		}

		guard let modifiedURL = components.url else {
			assertionFailure("Cannot make url from url components.")
			return url
		}

		return modifiedURL
	}

	static func makeBody<T>(from body: T) -> Data? where T: Encodable {
		do {
			return try JSONEncoder.movieDB.encode(body)
		} catch {
			assertionFailure(error.localizedDescription)
			return nil
		}
	}

	static func makeURLItems<T>(from parameters: T) -> [URLQueryItem] where T: Encodable & URLParameters {
		do {
			let data = try JSONEncoder.movieDB.encode(parameters)
			let dictionary = try JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as? [String: Any] ?? [:]
			return dictionary.map { URLQueryItem(name: $0.key, value: "\($0.value)") }
		} catch {
			assertionFailure(error.localizedDescription)
			return []
		}
	}

	static func makeURLRequest(url: URL, body: Data?) -> URLRequest {
		var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: RemoteConfiguration.TimeInterval.default)
		request.addValue("Accept", forHTTPHeaderField: "application/json")
		request.httpBody = body
		request.httpMethod = "GET"
		return request
	}

	static func makeResult<C, E>(
		data: Data?,
		converter: C,
		response: URLResponse?,
		error: Error?,
		completion: @escaping ValueClosure<Result<C.Value, E>>
	) where C: DataConverting, E: Decodable & Error {
		guard
			let data = data,
			let rawStatusCode = (response as? HTTPURLResponse)?.statusCode,
			let statusCode = HTTPStatusCode(rawValue: rawStatusCode)
		else { return }

		switch statusCode {
		case .ok:
			do {
				let value = try converter.convert(data: data)
				DispatchQueue.main.async {
					return completion(.success(value))
				}

			} catch {
				assertionFailure(error.localizedDescription)
				return
			}

		case .unauthorized:
			do {
				let value = try JSONDecoder.movieDB.decode(E.self, from: data)
				DispatchQueue.main.async {
					return completion(.failure(value))
				}

			} catch {
				assertionFailure(error.localizedDescription)
				return
			}
		}
	}
}
