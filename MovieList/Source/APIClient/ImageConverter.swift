//
//  ImageConverter.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import UIKit

struct ImageConverter: DataConverting {

	enum Error: Swift.Error {
		case cannotCreateImageFromData
	}

	func convert(data: Data) throws -> UIImage {
		guard let image = UIImage(data: data) else {
			throw Error.cannotCreateImageFromData
		}

		return image
	}
}
