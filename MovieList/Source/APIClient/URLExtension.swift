//
//  URLExtension.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 24/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

extension URL {

	init?(baseURLString: String, version: String, endpoint: Endpoint) {
		self.init(string: "\(baseURLString)/\(version)/\(endpoint.rawValue)")
	}

	init?(posterBaseURLString: String, size: PosterSize, path: String) {
		self.init(string: "\(posterBaseURLString)/\(size.rawValue)\(path)")
	}
}
