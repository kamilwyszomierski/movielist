//
//  DataConverting.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

protocol DataConverting {

	associatedtype Value

	func convert(data: Data) throws -> Value
}
