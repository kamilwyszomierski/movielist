//
//  FavouriteMoviesStorage.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

struct FavouriteMoviesStorage: FavouriteMoviesStorageProtocol {

	private enum Key: String {
		case favouriteMovies
	}

	private let defaults = UserDefaults.standard

	func getIDs(completion: @escaping ValueClosure<Result<[ID<Movie>], Swift.Error>>) {
		let ids = readIDs()
		completion(.success(ids))
	}

	func setMovie(with id: ID<Movie>, asFavourite isFavourite: Bool) {
		var ids = readIDs()
		if isFavourite {
			guard !ids.contains(id) else { return }
			ids.append(id)
		} else {
			ids.removeAll { $0 == id }
		}

		saveIDs(ids)
	}

	private func readIDs() -> [ID<Movie>] {
		let rawIDs = defaults.array(forKey: Key.favouriteMovies.rawValue) as? [Int] ?? []
		return rawIDs.map { ID<Movie>(rawValue: $0) }
	}

	private func saveIDs(_ ids: [ID<Movie>]) {
		let rawIDs = ids.map { $0.rawValue }
		defaults.set(rawIDs, forKey: Key.favouriteMovies.rawValue)
	}
}
