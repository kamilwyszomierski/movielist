//
//  FavouriteMoviesStorageProtocol.swift
//  MovieList
//
//  Created by Kamil Wyszomierski on 26/07/2020.
//  Copyright © 2020 Kamil Wyszomierski. All rights reserved.
//

import Foundation

protocol FavouriteMoviesStorageProtocol {

	func getIDs(completion: @escaping ValueClosure<Result<[ID<Movie>], Error>>)
	func setMovie(with id: ID<Movie>, asFavourite isFavourite: Bool)
}
